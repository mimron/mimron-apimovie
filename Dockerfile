FROM node:alpine

RUN mkdir -p /usr/src/mimron-apimovie && chown -R node:node /usr/src/mimron-apimovie

WORKDIR /usr/src/mimron-apimovie

COPY package.json yarn.lock ./

USER node

RUN yarn install --pure-lockfile

COPY --chown=node:node . .

EXPOSE 3000
